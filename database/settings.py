from database.database_helper import Database
from directory_manager import DirectoryManager


class Settings(Database):
    DB_TABLE_NAME_SETTINGS = "settings"

    DB_FIELD_ID = 'id'
    DB_FIELD_VALUE = 'value'

    ID_SETTINGS_OUTPUT_DIR = "OUTPUT_DIR"
    ID_SETTINGS_TEMP_OUTPUT_DIR = "TEMP_OUTPUT_DIR"
    ID_SETTINGS_CACHE_DIR = "CACHE_DIR"
    ID_SETTINGS_LOG_DIR = "LOG_DIR"
    ID_SETTINGS_TEMP_DIR = "TEMP"
    ID_SETTINGS_SELENIUM_DRIVER_PATH = "SELENIUM_DRIVER_PATH"
    ID_SETTINGS_SELENIUM_LOG_PATH = "SELENIUM_LOG_PATH"
    ID_SETTINGS_MAX_RETRY_COUNT = "MAX_RETRY_COUNT"

    directory_manager = DirectoryManager()
    DEFAULT_VALUE_SETTINGS_RELATIVE_OUTPUT_DIR = directory_manager.get_project_directory() + "/../../output"
    DEFAULT_VALUE_SETTINGS_RELATIVE_TEMP_OUTPUT_DIR = directory_manager.get_project_directory() + "/../../temp_output"
    DEFAULT_VALUE_SETTINGS_RELATIVE_CACHE_DIR = directory_manager.get_project_directory() + "/../../cache"
    DEFAULT_VALUE_SETTINGS_RELATIVE_LOG_DIR = directory_manager.get_project_directory() + "/../../log"
    DEFAULT_VALUE_SETTINGS_RELATIVE_TEMP_DIR = directory_manager.get_project_directory() + "/../../temp"
    DEFAULT_VALUE_SETTINGS_RELATIVE_SELENIUM_DRIVER_PATH = directory_manager.get_project_directory() + "/chromedriver"
    DEFAULT_VALUE_SETTINGS_RELATIVE_SELENIUM_LOG_PATH = directory_manager.get_project_directory() + "/../../log/geckodriver.log"
    DEFAULT_VALUE_SETTINGS_RELATIVE_MAX_RETRY_COUNT = 4

    def get_setting_value_for(self, id):
        """
            Fetches the value of setting for the id provided
            :returns:
                Response for the query.
            :arg:
                id - Statement to be executed.
                attributes - Attributes supporting the statement.
        """
        output = self.query(
            "SELECT `" + Settings.DB_FIELD_VALUE + "` FROM `" + Settings.DB_TABLE_NAME_SETTINGS + "` WHERE `" + Settings.DB_FIELD_ID + "` = %s",
            (id,))
        if output is not None and len(output) > 0 and len(output[0]) > 0:
            return output[0][0]
        else:
            return None

    def update_setting_value_for(self, id, value):
        """
            Fetches the value of setting for the id provided
            :returns:
                Response for the query.
            :arg:
                id - Statement to be executed.
                attributes - Attributes supporting the statement.
        """
        output = self.execute(
            "INSERT INTO `" + Settings.DB_TABLE_NAME_SETTINGS + "` (`" + Settings.DB_FIELD_ID + "`, `" + Settings.DB_FIELD_VALUE + "`) VALUES (%s, %s) ON DUPLICATE KEY UPDATE `" + Settings.DB_FIELD_ID + "` = %s, `" + Settings.DB_FIELD_VALUE + "` = %s",
            (id, value, id, value))
        if output is not None and len(output) > 0 and len(output[0]) > 0:
            return output[0][0]
        else:
            return None

    @staticmethod
    def get_default_setting_for(id):
        """
            Returns the default setting for the specified id.
            :returns:
                deafult setting value for the id
            :arg:
                id - Statement to be executed.
        """
        map = {
            Settings.ID_SETTINGS_OUTPUT_DIR: Settings.DEFAULT_VALUE_SETTINGS_RELATIVE_OUTPUT_DIR,
            Settings.ID_SETTINGS_CACHE_DIR: Settings.DEFAULT_VALUE_SETTINGS_RELATIVE_CACHE_DIR,
            Settings.ID_SETTINGS_LOG_DIR: Settings.DEFAULT_VALUE_SETTINGS_RELATIVE_LOG_DIR,
            Settings.ID_SETTINGS_SELENIUM_DRIVER_PATH: Settings.DEFAULT_VALUE_SETTINGS_RELATIVE_SELENIUM_DRIVER_PATH,
            Settings.ID_SETTINGS_SELENIUM_LOG_PATH: Settings.DEFAULT_VALUE_SETTINGS_RELATIVE_SELENIUM_LOG_PATH,
            Settings.ID_SETTINGS_MAX_RETRY_COUNT: Settings.DEFAULT_VALUE_SETTINGS_RELATIVE_MAX_RETRY_COUNT,
            Settings.ID_SETTINGS_TEMP_DIR: Settings.DEFAULT_VALUE_SETTINGS_RELATIVE_TEMP_DIR,
            Settings.ID_SETTINGS_TEMP_OUTPUT_DIR: Settings.DEFAULT_VALUE_SETTINGS_RELATIVE_TEMP_OUTPUT_DIR

        }

        return map.get(id, None)

    def create_table(self):
        """
        Creates table if it does not exist.
        :return:
        """
        try:
            statement = "CREATE TABLE IF NOT EXISTS `" + self.DB_TABLE_NAME_SETTINGS + "` (`" + self.DB_FIELD_ID + "` VARCHAR(64), `" + self.DB_FIELD_VALUE + "` TEXT, PRIMARY KEY(`" + self.DB_FIELD_ID + "`))"
            self.execute(statement, None)
        except Exception as e:
            pass