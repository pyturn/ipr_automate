import mysql.connector

DB_HOST = "localhost"
DB_USER = "root"
DB_PASS = ""
DB_DATABASE = "ipr"


class Database(object):
    _db_connection = None

    def connect(self):
        """
        Connects to the database, sets the local variable and returns it.
        :returns:
            Available Connection.
        """
        self._db_connection = mysql.connector.connect(
            host=DB_HOST,
            user=DB_USER,
            passwd=DB_PASS,
            database=DB_DATABASE
        )
        return self._db_connection

    def disconnect(self):
        """
            Disconnects from the current connection.
        """
        if self._db_connection is not None:
            self._db_connection.close()
            self._db_connection = None

    def query(self, statement, attributes):
        """
            Query the database for response
            :returns:
                Response for the query.
            :arg:
                statement - Statement to be executed.
                attributes - Attributes supporting the statement.
        """
        # if not connected to the database connect
        if self._db_connection is None:
            self.connect()
        cursor = self._db_connection.cursor()
        cursor.execute(statement, attributes)
        response = cursor.fetchall()
        cursor.close()
        del cursor
        return response

    def execute(self, statement, attributes):
        """
            Execute a query for the database
            :arg:
                statement - Statement to be executed.
                attributes - Attributes supporting the statement.
        """
        if self._db_connection is None:
            self.connect()
        cursor = self._db_connection.cursor()
        cursor.execute(statement, attributes)
        self._db_connection.commit()
        t = cursor.rowcount
        cursor.close()
        del cursor
        #return t

