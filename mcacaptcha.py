from keras.models import load_model
from helpers import resize_to_fit
import numpy as np
import cv2
import pickle
from directory_manager import DirectoryManager

class MCACaptcha(object):
    """
    This is captcha for the MCA captcha website.
    Is used to solve te captcha given the image as the input.
    Singleton class and is loaded the first time it is called, for optimization purpose.
    """
    _instance = None
    directory_manager = DirectoryManager()
    IMAGE_FILE_EXTENSION = '.png'
    MODEL_FILENAME = directory_manager.get_project_directory() + "/captcha_model.hdf5"
    MODEL_LABELS_FILENAME = directory_manager.get_project_directory() + "/model_labels.dat"

    def __new__(cls):
        if not cls._instance:
            cls._instance = super(MCACaptcha, cls).__new__(cls)
        return cls._instance

    @staticmethod
    def get_model():
        """
            :returns:
                model for captcha solving.
        """
        model = load_model(MCACaptcha.MODEL_FILENAME)
        return model

    @staticmethod
    def get_label():
        """
            :returns:
                labels for the model.
        """
        with open(MCACaptcha.MODEL_LABELS_FILENAME, "rb") as f:
            lb = pickle.load(f)
        return lb

    def predict(self, image_file):
        """
            :returns:
                solved text for the captcha image.
            :arg:
                image_file - Absolute path of the image file.
        """
        model = self.get_model()
        lb = self.get_label()
        image = cv2.imread(image_file, cv2.IMREAD_GRAYSCALE)
        cropped_image = image[5:72, 5:242]
        image = cv2.resize(cropped_image, (237, 67))

        # threshold the image (convert it to pure black and white)
        thresh = cv2.threshold(image, 127, 255, cv2.THRESH_BINARY)[1]

        letter_positions = []
        x_pos = 0
        letter_flag = 0
        for i in range(0, 236):
            black_pixels = 67 - cv2.countNonZero(thresh[:, i:i + 1])
            # print(black_pixels)
            if black_pixels < 2:
                if letter_flag == 1 and i - x_pos >= 8:
                    letter_positions.append([x_pos - 1, i])
                    letter_flag = 0
            else:
                if letter_flag == 0:
                    x_pos = i
                    letter_flag = 1

        predictions = []
        for i in range(0, len(letter_positions)):
            # Grab the coordinates of the letter in the image
            x1, x2 = letter_positions[i]
            if x1 < 0: x1 = 0
            letter_image = image[:, x1:x2]
            # Re-size the letter image to 20x20 pixels to match training data
            letter_image = resize_to_fit(letter_image, 40, 40)

            # Turn the single image into a 4d list of images to make Keras happy
            letter_image = np.expand_dims(letter_image, axis=2)
            letter_image = np.expand_dims(letter_image, axis=0)

            # Ask the neural network to make a prediction
            prediction = model.predict(letter_image)

            # Convert the one-hot-encoded prediction back to a normals letter
            letter = lb.inverse_transform(prediction)[0]
            predictions.append(letter)

        # Print the captcha's text
        captcha_text = "".join(predictions)
        return captcha_text
