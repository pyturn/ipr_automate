import cv2
import pdb
import numpy as np
import pytesseract
import argparse
import string
import random
import os


config = ('-l eng --oem 1 --psm 8')
kernel = np.ones((2,2), np.uint8) 

def sort_contours(cnts, method="left-to-right"):
    # initialize the reverse flag and sort index
    reverse = False
    i = 0
    # handle if we need to sort in reverse
    if method == "right-to-left" or method == "bottom-to-top":
        reverse = True
    # handle if we are sorting against the y-coordinate rather than
    # the x-coordinate of the bounding box
    if method == "top-to-bottom" or method == "bottom-to-top":
        i = 1
    # construct the list of bounding boxes and sort them from top to
    # bottom
    boundingBoxes = [cv2.boundingRect(c) for c in cnts]
    (cnts, boundingBoxes) = zip(*sorted(zip(cnts, boundingBoxes),
        key=lambda b:b[1][i], reverse=reverse))
    # return the list of sorted contours and bounding boxes
    return cnts


def remove_special_characters(string):
    return ''.join(e for e in string if e.isalnum())


def solve_captcha(imPath , temp_captacha_dir):
    """
    Denoise the given image and saves it to temp directory. 
    param imPath: absolute path of image.
    temp_captcha_dir : Directory where image needs to be saved. 
    return :
    text : Predicted value of captcha. 

    """

    image = cv2.imread(imPath)
    imgray = cv2.cvtColor(image,cv2.COLOR_BGR2GRAY)
    ret,thresh = cv2.threshold(imgray,127,255,0)
    im2, contours, hierarchy = cv2.findContours(thresh,cv2.RETR_TREE,cv2.CHAIN_APPROX_SIMPLE)
    contours = sorted(contours, key=cv2.contourArea, reverse=True)
    contours = sort_contours(contours[0:6] , "left-to-right")

    img = [None] * 5
    for j in range(1,6):
        x,y,w,h = cv2.boundingRect(contours[j])
        img[j-1] = thresh[y:y+h, x:x+w]
        img[j-1] = cv2.resize(img[j-1], (30, 35))
        img[j-1] = cv2.copyMakeBorder(img[j-1] , 10,10,10,10,cv2.BORDER_CONSTANT , value = [255,255,255]) 
        # cv2.imshow('Window ',img[j-1])
        # key = cv2.waitKey(0) & 0xFF
        # if key == ord("q"):
        #   continue

    vis = np.concatenate((img[0],img[1],img[2],img[3],img[4]), axis=1)
        
    img_erosion = cv2.erode(vis, kernel, iterations=1)  
    img_dilation = cv2.dilate(vis, kernel, iterations=1)
    img_dilation_2 = cv2.dilate(img_erosion, kernel, iterations=1)

    cv2.imwrite( temp_captacha_dir + "/Saved_Image.jpg", img_dilation_2)

    saved_image = cv2.imread(temp_captacha_dir + "/Saved_Image.jpg")
    #cv2.imshow('Window',saved_image)
    #cv2.waitKey(2000)
    text = pytesseract.image_to_string(saved_image, config=config)
    text = remove_special_characters(text) 
    return text


