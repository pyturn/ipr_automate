import os


class DirectoryManager:

    def get_project_directory(self):
        return os.path.dirname(__file__)
