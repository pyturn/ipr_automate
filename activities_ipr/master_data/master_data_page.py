from PIL import Image
import string
import random
import os
import urllib.request
import pdb
import requests 
import shutil
from io import BytesIO
#from mcacaptcha import MCACaptcha
from solve_captcha_using_tesseract import solve_captcha
from automation_process import AutomationProcess
from automation_process import AutomationInterrupted
from selenium.common.exceptions import NoAlertPresentException
from database.settings import Settings
import time
from database.database_helper import Database
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver import ActionChains 


class Automate(AutomationProcess):
    """
    This class is for Automation. Different methods are available to automate the
    interaction with web.
    """

    URL = 'http://ipindiaonline.gov.in/eregister/viewdetails_new.aspx'

    settings_db = None
    output_dir = None
    temp_captcha_dir = None
    success_captcha_dir = None
    failure_captcha_dir = None
    mca_captcha_solver = None
    max_retry_count = 4

    def __init__(self):
        """
        Creating browser object and opening the web browser
        """
        super().__init__()
        self.settings_db = Settings()
        cache_dir = self.settings_db.get_setting_value_for(Settings.ID_SETTINGS_CACHE_DIR)
        self.temp_captcha_dir = cache_dir + "/ipr_captcha/temp"
        self.success_captcha_dir = cache_dir + "/ipr_captcha/success"
        self.failure_captcha_dir = cache_dir + "/ipr_captcha/failure"

        directories = [self.temp_captcha_dir, self.success_captcha_dir, self.failure_captcha_dir]
        for dir in directories:
            if not os.path.isdir(dir):
                os.makedirs(dir)
        self.output_dir = self.settings_db.get_setting_value_for(Settings.ID_SETTINGS_OUTPUT_DIR)
        self.temp_dir = self.settings_db.get_setting_value_for(Settings.ID_SETTINGS_TEMP_DIR)
        #self.mca_captcha_solver = self.settings_db.get_setting_value_for(Settings.ID_SETTINGS_MAX_RETRY_COUNT)
        #self.mca_captcha_solver = MCACaptcha()
        self.init_browser()

    def search_for_tm(self, tm):
        """
        Automation process for a single tm
        :exception: Exceptions should be handled at a global scale and should be indexed in database if it happens.
        :param tm: tm for which the automation is to be performed.
        :return:
        """
        
        #pdb.set_trace()
        for i in range(0,self.max_retry_count):
            temp_image_file_name = self.get_captcha(tm)
            captcha_text = solve_captcha(temp_image_file_name , self.temp_captcha_dir)
            self.fill_captcha(captcha_text)
            #time.sleep(5)
            self.click_search_button()
            time.sleep(1.7)
            visible_alert_dialog_id = self.is_application_id_invalid()
            application_id_status = visible_alert_dialog_id is None
            if(application_id_status):
                if(self.is_alert_present()):
                    success_status = False
                    self.save_captcha_for_training(success_status, temp_image_file_name, captcha_text)
                    self.refresh_captcha()
                    continue
                else:
                    success_status = True
                    self.save_captcha_for_training(success_status, temp_image_file_name, captcha_text)
                    return 1
            else:
                success_status = False
                self.save_captcha_for_training(success_status, temp_image_file_name, captcha_text)
                return 0
        return 2

    def refresh_captcha(self):
        """
        Refreshes captcha if it fails to fill the correct one.
        """
        self.browser.find_element_by_id("ImageButton1").click()

    def is_alert_present(self):
        """
        Function to check if alert is present or not after filling captcha. If alert is present it dismisses it. 
        return : 
        True : If alert is present.
        False : If alert is not present.
        """

        try:
            alert = self.browser.switch_to_alert()
            alert.dismiss()
            return True 
        except NoAlertPresentException: 
            return False

    def reach_url(self, url):
        """
        Reach to the given URL
        :arg:
        url - URL of the websote to visit.
        """
        self.browser.get(url)

    def id_generator(self, size=10, chars=string.ascii_lowercase + string.digits):
        """
        Generates random text used for ids and filenames.
        :returns:
        Random text generated.
        """
        return ''.join(random.choice(chars) for _ in range(size))

    def fill_tm(self, tm_no):
        """
        Fills the tm in the required text box.
        :arg:
        tm_no - Required tm value.
        """
        #Selecting National IRDI no
        #pdb.set_trace()
        self.browser.maximize_window()
        self.browser.find_element_by_id("rdb_0").click()
        tm_element = self.browser.find_element_by_id("applNumber")
        tm_element.send_keys(Keys.CONTROL, 'a')
        tm_element.send_keys(tm_no)
        
    def get_captcha(self ,tm):
        """
        Captures the image for the captcha. It moves the image to bin_success , bin_failure, original folder.
        :returns:
        Absolute path of the image file for the captcha.
        """
        # now that we have the preliminary stuff out of the way time to get that image :D
        element = self.browser.find_element_by_xpath('//*[@id="panelmain"]/table/tbody/tr[3]/td[2]/div/img')  # find part of the page you want image of
        location = element.location
        size = element.size
        png = self.browser.get_screenshot_as_png()  # saves screenshot of entire page

        rgba = Image.open(BytesIO(png))  # uses PIL library to open image in memory
        im = rgba.convert('RGB')
        left = location['x']
        top = location['y']
        right = location['x'] + size['width']
        bottom = location['y'] + size['height']

        im = im.crop((left, top, right, bottom))  # defines crop points
        random_name = self.id_generator()
        temp_captcha_file_name = self.temp_captcha_dir + "/" + tm + "_" + random_name +'.jpg'
        im.save(temp_captcha_file_name)  # saves new cropped image
        return temp_captcha_file_name

    # def solve_captcha(self, image_path):
    #     """
    #     Solves the captcha.
    #     :returns
    #     Solution to the captcha.
    #     :arg:
    #     image_path: Absolute path of the image.
    #     """
    #     return self.mca_captcha_solver.predict(image_path)

    def fill_captcha(self, captcha_text):
        """
        Fills the captcha in the required text box.
        :arg:
        captcha_text: Captcha wehich need to be entered.
        """
        self.browser.find_element_by_id('txtCaptcha').clear()
        self.browser.find_element_by_id('txtCaptcha').send_keys(format(captcha_text))

    def click_search_button(self):
        """
        Clicks the button which searches the page for the content corresponding the company with tm in the text box.
        """
        self.browser.find_element_by_id('btnView').click()

    def is_application_id_invalid(self):
        """
        Used to check if a dialog box is present on the screen or not.

        :returns:
        The type of messagebox being displayed.
        alertbg_overlay - Succesful / No messgae box

        """
        try:
            elem1 = self.browser.find_element_by_id('panel_Error')
            if (elem1.is_displayed() == False):
                raise ValueError
            else:
                return 'panel_Error'
        except:
                return None

    def dismiss_alert_dialog(self):
        """
        Dismisses any present dialog for the master page.
        :return:
        """
        try:
            close_button = self.browser.find_element_by_id("msgboxclose")
            close_button.click()
        except:
            webdriver.ActionChains(self.browser).send_keys(Keys.ESCAPE).perform()

    def refresh_captcha(self):
        try:
            refresh_button = self.browser.find_element_by_id("captchaRefresh")
            refresh_button.click()
        except:
            pass

    def save_captcha_for_training(self, success_status, image_path, solution):
        """
        Saves the images that can be used for training at later stages.
        :param success_status: Whether the captcha was solved or not.
        :param image_path: The current absolute path of the captcha image.
        :param solution: The output of solving the captcha image.
        :return:
        """
        if success_status:
            os.rename(image_path, self.success_captcha_dir + "/" + solution + '.jpg')
        else:
            os.rename(image_path, self.failure_captcha_dir + "/" + solution + '.jpg')

    def save_page(self , tm , suffix):
        """
        """
        a = self.get_page_source()
        name = self.temp_dir + "/" + tm + suffix +".html"
        file = open(name,'w',encoding='utf-8')
        file.write(a)
        file.close()
        try:
            os.rename(name , self.output_dir + "/" + tm + suffix + ".html")
            return True
        except:
            return False

    def save_image(self , tm):
        """
        Saves the image present on application page into output directory.
        :return
        True : If it is able to fetch all the images.
        False : If not able to fetch all the images. 
        """
        #pdb.set_trace()
        try:
            images = self.browser.find_elements_by_tag_name('img')
        except:
            return True
        i = 1

        for img in images:
            try:
                src = img.get_attribute('src')
                all_cookies = self.browser.get_cookies()
                cookie = all_cookies[0]
                mycookie = {cookie['name']: cookie['value']}
                r = requests.get(src, stream=True, cookies =  mycookie)
                if (r.status_code == 200):
                    with open( self.output_dir + "/" + tm + "_" + str(i) + ".jpeg", 'wb') as f:
                        r.raw.decode_content = True
                        shutil.copyfileobj(r.raw, f) 
                i = i+1
            except:
                return False
        return True






    def get_next_page(self):
        """
        Clicks on the application number and helps to get on the last page. 
        return:
        True : If able to get on the last page.
        False : If some exception occoured and not able to get on the last page. 
        """
        try:
            self.browser.find_element_by_id("SearchWMDatagrid_ctl03_lnkbtnappNumber1").click()
            self.browser.find_element_by_id("lblappdetail").click()
            return True
        except:
            return False



    def get_page_source(self):
        """
        Fetches the page source for the current state.
        :return:
        """
        return self.browser.page_source

    def quit_browser(self):
        """
        Quit the browser window.
        """
        self.settings_db.disconnect()
        self.browser.quit()


class MasterDataDatabaseHelper(Database):
    DB_TABLE_NAME_tm_LOG = "tm_master_data"

    DB_FIELD_tm = "tm"
    #DB_FIELD_RETRY_COUNT = "retry_count"
    #DB_FIELD_PRIORITY = "priority"
    DB_FIELD_STATUS = "status"
    #DB_FIELD_ERROR_MSG = "error_msg"

    DEFAULT_PRIORITY = 2
    STATUS_PENDING = "PENDING"
    STATUS_INVALID = "INVALID"
    STATUS_SUCCESS = "SUCCESS"
    STATUS_FAILED = "FAILED"

    db = None

    def mark_success(self, tm):
        """
        Marks a tm successfully executed.
        :param tm: tm
        :return:
        """
        statement = "UPDATE `" + MasterDataDatabaseHelper.DB_TABLE_NAME_tm_LOG + "` SET `" + \
                    MasterDataDatabaseHelper.DB_FIELD_STATUS + "` = '" + MasterDataDatabaseHelper.STATUS_SUCCESS + \
                    "' WHERE `" + MasterDataDatabaseHelper.DB_FIELD_tm + "` = %s"
        print(statement)
        self.execute(statement, (tm,))

    def mark_invalid(self, tm):
        statement = "UPDATE `" + MasterDataDatabaseHelper.DB_TABLE_NAME_tm_LOG + "` SET `" + \
                    MasterDataDatabaseHelper.DB_FIELD_STATUS + "` = '" + MasterDataDatabaseHelper.STATUS_INVALID + \
                    "' WHERE `" + MasterDataDatabaseHelper.DB_FIELD_tm + "` = %s"
        print(statement)
        self.execute(statement, (tm,))

    def mark_failed(self, tm):
        """
        Marks a tm failed to execute.
        :param error_msg: error message stating the reason it failed.
        :param tm: tm
        :return:
        """
        statement = "UPDATE `" + MasterDataDatabaseHelper.DB_TABLE_NAME_tm_LOG + "` SET `" + \
                    MasterDataDatabaseHelper.DB_FIELD_STATUS + "` = '" + MasterDataDatabaseHelper.STATUS_FAILED + \
                    "' WHERE `" + MasterDataDatabaseHelper.DB_FIELD_tm + "` = %s"
        print(statement)
        self.execute(statement, (tm,))

    def queue_tm(self, tm_list):
        """
        Queues the list of tm for processing/download.
        :param tm_list: List of tm
        :return:
        """
        # creates a table if it does not exist
        self.create_table()

        # populate the tm list in the database
        for tm in tm_list:
            try:
                # There maybe a duplicate entry or other exceptions. Principle of greater good.
                statement = \
                    "INSERT INTO `" + MasterDataDatabaseHelper.DB_TABLE_NAME_tm_LOG + "` " \
                    "(`" + MasterDataDatabaseHelper.DB_FIELD_tm + "`, `" + MasterDataDatabaseHelper.DB_FIELD_STATUS + "`) VALUES" \
                    "(%s, %s)"
                self.execute(statement, (tm,self.STATUS_PENDING))
            except Exception as e:
                print(e)

    def get_all_pending_tm(self):
        """
        Chooses the tms to be processed next. Choice is based in its status, priority and retry count.
        :return: A single tm to be processed next.
        """
        statement = \
            "SELECT `" + MasterDataDatabaseHelper.DB_FIELD_tm + "` FROM `" + MasterDataDatabaseHelper.DB_TABLE_NAME_tm_LOG + "`" \
            " WHERE `" + MasterDataDatabaseHelper.DB_FIELD_STATUS + "` != '" + MasterDataDatabaseHelper.STATUS_SUCCESS+"'"
        tm_list = self.query(statement, None)
        if tm_list is not None and len(tm_list) > 0 and len(tm_list[0]) > 0:
            converted_tm_list = []
            for tm in tm_list:
                converted_tm_list.append(tm[0])
            return converted_tm_list
        else:
            return []

    def get_single_pending_tm(self):
        """
        Chooses the tm to be processed next. Choice is based in its status, priority and retry count.
        :return: A list of tms to be processed next.
        """
        statement = \
            "SELECT `" + MasterDataDatabaseHelper.DB_FIELD_tm + "` FROM `" + MasterDataDatabaseHelper.DB_TABLE_NAME_tm_LOG + "`" \
            " WHERE `" + MasterDataDatabaseHelper.DB_FIELD_STATUS + "` != '" + MasterDataDatabaseHelper.STATUS_SUCCESS + "' AND `" + MasterDataDatabaseHelper.DB_FIELD_RETRY_COUNT + "` < 10 " \
            "ORDER BY CASE WHEN `" + MasterDataDatabaseHelper.DB_FIELD_STATUS + "` = '" + MasterDataDatabaseHelper.STATUS_PENDING + "' THEN 0 " \
            "WHEN `" + MasterDataDatabaseHelper.DB_FIELD_STATUS + "` = '" + MasterDataDatabaseHelper.STATUS_RETRY + "' THEN 1 " \
            "WHEN `" + MasterDataDatabaseHelper.DB_FIELD_STATUS + "` = '" + MasterDataDatabaseHelper.STATUS_FAILED + "' THEN 2 ELSE 3 END ASC, " \
            "`" + MasterDataDatabaseHelper.DB_FIELD_PRIORITY + "` ASC LIMIT 1"
        tm = self.query(statement, None)
        if tm is not None and len(tm) > 0 and len(tm[0]) > 0:
            return tm[0][0]
        else:
            return None

    def create_table(self):
        """
        Creates table if it does not exist.
        :return:
        """
        pdb.set_trace()
        try:
            statement = "CREATE TABLE IF NOT EXISTS `" + MasterDataDatabaseHelper.DB_TABLE_NAME_tm_LOG + "` (`" + MasterDataDatabaseHelper.DB_FIELD_tm + "` VARCHAR(64),`"  + MasterDataDatabaseHelper.DB_FIELD_STATUS + "` VARCHAR(32), " + "PRIMARY KEY (`" + MasterDataDatabaseHelper.DB_FIELD_tm + "`))"
            print(statement)
            self.execute(statement, None)
        except Exception as e:
            pass
