from activities_ipr.master_data import master_data_page
from automation_process import AutomationInterrupted
import pdb

# Initiating Firefox Web Browser by creating the object
master_page = master_data_page.Automate()
master_page_database = master_data_page.MasterDataDatabaseHelper()

PRELIM = 'prelim'
MAIN = 'main'

# For queueing purpose.
tm_list = ['1343002','1486029','1493421','1516437','1518069','1587271','1595638','1597302','1650143',
            '1655569','1670040','1698981','1705546','1711685','1723722','1724032','1742409','1742417',
            '1749175','1749455','1753218','1759707','1760754','1760980','1762853','1767410']
master_page_database.queue_tm(tm_list)
tm_list = master_page_database.get_all_pending_tm()

#tm = master_page_database.get_single_pending_tm()
for tm in tm_list:
    #pdb.set_trace()
    master_page.reach_url(master_page.URL)
    master_page.fill_tm(tm)
    status = master_page.search_for_tm(tm)

    if(status == 0):
        master_page_database.mark_invalid(tm)
        continue
    elif(status == 2):
        master_page_database.mark_failed(tm)
        continue
    else:
        save_page_status = master_page.save_page(tm , PRELIM)
        if(save_page_status):
            next_page_status = master_page.get_next_page()
            if(next_page_status):
                save_page_status = master_page.save_page(tm , MAIN)
                save_image_status = master_page.save_image(tm)
                if(save_page_status and save_image_status):
                    master_page_database.mark_success(tm)
                else:
                    master_page_database.mark_failed(tm)

master_page.quit_browser()