from database.settings import Settings
import os
from selenium.webdriver import Chrome
from selenium import webdriver


class AutomationProcess:
    """
        Automation process is a process used for automation.
        This is an abstract class. It prepares the things associated with automation process.
    """

    browser = None

    def __init__(self):
        self.check_other_settings()
        self.check_directories()

    @staticmethod
    def check_directories():
        """
        Fetches and checks the directories if they exist and if not creates them.
        """
        # fetch the default directories associated with the automation process
        settings_db = Settings()
        settings_db.create_table()
        settings = [
            Settings.ID_SETTINGS_CACHE_DIR,
            Settings.ID_SETTINGS_OUTPUT_DIR,
            Settings.ID_SETTINGS_LOG_DIR,
            Settings.ID_SETTINGS_TEMP_DIR]

        directories = []
        for setting_id in settings:
            setting_value = settings_db.get_setting_value_for(setting_id)
            if setting_value is not None:
                directories.append(setting_value)
            else:
                settings_db.update_setting_value_for(setting_id, settings_db.get_default_setting_for(setting_id))
                directories.append(settings_db.get_default_setting_for(setting_id))

        # got all the directories now proceed to making them
        for dir in directories:
            if not os.path.isdir(dir):
                os.makedirs(dir)

        # disconnect the database
        settings_db.disconnect()

    @staticmethod
    def check_other_settings():
        """
        Fetches and checks the directories if they exist and if not creates them.
        """
        settings_db = Settings()
        settings_db.create_table()

        settings = [
            Settings.ID_SETTINGS_SELENIUM_DRIVER_PATH,
            Settings.ID_SETTINGS_SELENIUM_LOG_PATH,
            Settings.ID_SETTINGS_MAX_RETRY_COUNT]

        for setting_id in settings:
            setting_value = settings_db.get_setting_value_for(setting_id)
            if setting_value is None:
                settings_db.update_setting_value_for(setting_id, settings_db.get_default_setting_for(setting_id))

        # disconnect the database
        settings_db.disconnect()

    def init_browser(self):
        """
        Initiates the browser
        """
        if self.browser is None:
            settings_db = Settings()
            driver_path = settings_db.get_setting_value_for(Settings.ID_SETTINGS_SELENIUM_DRIVER_PATH)
            if driver_path is None:
                # update default value and use the default
                driver_path = settings_db.get_default_setting_for(Settings.ID_SETTINGS_SELENIUM_DRIVER_PATH)
                settings_db.update_setting_value_for(Settings.ID_SETTINGS_SELENIUM_DRIVER_PATH, driver_path)

            driver_log_path = settings_db.get_setting_value_for(Settings.ID_SETTINGS_SELENIUM_LOG_PATH)
            if driver_log_path is None:
                # update default value and use the default
                driver_log_path = settings_db.get_default_setting_for(Settings.ID_SETTINGS_SELENIUM_LOG_PATH)
                settings_db.update_setting_value_for(Settings.ID_SETTINGS_SELENIUM_LOG_PATH, driver_log_path)

            output_temp_path = settings_db.get_setting_value_for(Settings.ID_SETTINGS_TEMP_OUTPUT_DIR)
            if output_temp_path is None:
                # update default value and use the default.
                output_temp_path = settings_db.get_default_setting_for(Settings.ID_SETTINGS_TEMP_OUTPUT_DIR)
                settings_db.update_setting_value_for(Settings.ID_SETTINGS_TEMP_OUTPUT_DIR, output_temp_path)

            output_path = settings_db.get_setting_value_for(Settings.ID_SETTINGS_OUTPUT_DIR)
            if output_path is None:
                # update default value and use the default.
                output_path = settings_db.get_default_setting_for(Settings.ID_SETTINGS_OUTPUT_DIR)
                settings_db.update_setting_value_for(Settings.ID_SETTINGS_OUTPUT_DIR, output_path)
            output_path = "/home/abhinav/Desktop/IPRREPO/temp_output"
            print("output_path is ",output_path)

            chromeOptions = webdriver.ChromeOptions()
            prefs = {"download.default_directory" : output_path}
            chromeOptions.add_experimental_option("prefs",prefs)
            #chromeOptions.add_argument("download.default_directory="+output_path)
            self.browser = Chrome(executable_path=driver_path, chrome_options=chromeOptions)
            settings_db.disconnect()

class AutomationInterrupted(Exception):
    pass